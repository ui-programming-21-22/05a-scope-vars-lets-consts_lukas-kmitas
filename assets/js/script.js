console.log("hello from script");

function pageload() {
	let canvas = document.getElementById("mycanvas");
	let context = canvas.getContext("2d");

    context.beginPath(); 
    context.fillStyle = "Blue";
    context.fillRect(0, 0, 50, 50);
    context.stroke();

    context.beginPath(); 
    context.fillStyle = "Blue";
    context.fillRect(950, 0, 50, 50);
    context.stroke();

    context.beginPath(); 
    context.fillStyle = "Blue";
    context.fillRect(0, 650, 50, 50);
    context.stroke();

    context.beginPath(); 
    context.fillStyle = "Blue";
    context.fillRect(950, 650, 50, 50);
    context.stroke();

    context.beginPath(); 
	context.moveTo(150, 200);
	context.lineTo(200, 250);
	context.lineTo(300, 50);
    context.lineTo(500, 500);
    context.stroke();

    context.beginPath();
    context.arc(200, 300, 50, 50, Math.PI, true);
    context.stroke();

    context.font = "16px Arial";
    context.fillStyle = "Purple";
    context.fillText("Hippity Hoppity...", 400, 50);
    context.font = "16px Garamond";
    context.fillStyle = "Red";
    context.fillText("Mr Popo", 600, 400);
    context.font = "24px Brush Script MT";
    context.fillStyle = "Black";
    context.fillText("Next time on dragon ball Z", 600, 600);

    const grd = context.createLinearGradient(0, 0, 200, 0);
    grd.addColorStop(0, "blue");
    grd.addColorStop(1, "red"); 
    context.fillStyle = grd;
    context.fillRect(500, 200, 150, 150);

    const img = document.getElementById("image");
    context.drawImage(img, 50, 50, 50, 50);

    let greeting = "Holla amigo";
    if (true)
    {
        let greeting = "PaPa";
        console.log(greeting); 
    }
    console.log(greeting);   

}

pageload();